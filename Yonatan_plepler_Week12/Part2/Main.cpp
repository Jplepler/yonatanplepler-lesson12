#include "MessagesSender.h"
mutex msgsMutex;
mutex usersMutex;


enum OPT
{
	SIGN = 1,
	OUT,
	PRT,
	EXT
};

bool flag = false;

/*
This function reads from the data.txt file every 60 seconds and adds it to the messages queue.
In: refrence to the queue of messages
*/
void readFromData(queue<string>& msgsQueue)
{
	string line;
	ifstream myFile;
	myFile.open("data.txt");//open for reading

	while (myFile.peek() != ifstream::traits_type::eof())
	{
		
		if (myFile.is_open())
		{
			if (getline(myFile, line))//Read text
			{
				msgsMutex.lock();
				msgsQueue.push(line);//push data into queue of messages
				msgsMutex.unlock();
			}
			
			

		}


		this_thread::sleep_for(chrono::seconds(60));
	}
	myFile.close();//close for reading
	
	std::ofstream ofs;
	ofs.open("data.txt", std::ofstream::out | std::ofstream::trunc);
	ofs.close();
	
}


/*
This function sends a message to all users form the queue of messages
In: queue of messages and set of users
*/
void sendToOutput(queue<string>& msgsQueue, set<string>& users)
{
	int i = 0;
	ofstream outfile;
	set<string>::iterator it;
	outfile.open("output.txt", std::ios_base::app); // append instead of overwrite
	while (!::flag)
	{
		
		if (!msgsQueue.empty() && !users.empty())
		{
			//Send message to all users
			msgsMutex.lock();//Lock queue
			usersMutex.lock();//Lock users
			for (it = users.begin(); it != users.end(); ++it)
			{
				if (msgsQueue.front() != "" && msgsQueue.front() != " " && msgsQueue.front() != "\n")//good content filter
				{
					outfile << (*it) << ": " << msgsQueue.front() << endl;//Print message without removing f=it from queue
				}
			}
			usersMutex.unlock();
			msgsQueue.pop();//Remove message
			msgsMutex.unlock();//Unlock Queue
		}

	}
	outfile.close();
}


int main()
{
	unsigned int choice = 0;
	queue<string> msgsQueue;
	MessagerSender m;
	set<string> logged = m.getUsers();

	//Create the threads
	thread tRead(readFromData, ref(msgsQueue));
	thread tWrite(sendToOutput, ref(msgsQueue), ref(logged));
	

	do
	{
		
		m.printMenu();
		cin >> choice;
		system("cls");


		switch (choice)
		{
		case SIGN:
			m.signIn(usersMutex);
			logged = m.getUsers();
			break;

		case OUT:
			m.signOut(usersMutex);
			break;

		case PRT:
			m.connectedUsers(usersMutex);
			break;

		case EXT:
			system("cls");
			cout << "Bye Bye!" << endl;
			cout << "Program is shutting down, (maximum wait time : 60s)" << endl;
			break;

		default:
			cout << "Invalid input, try again" << endl;
			break;
		}

	} while (choice != EXT);
	
	::flag = true;//stop both threads (Global flag variable)

	tRead.join();
	tWrite.join();

	return 0;
}



