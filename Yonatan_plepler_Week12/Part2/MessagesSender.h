#pragma once
#include <set>
#include <string>
#include <iostream>
#include <algorithm>
#include <chrono>
#include <thread>
#include <mutex>
#include <fstream>
#include <queue>
using namespace std;




class MessagerSender 
{
public:
	MessagerSender();
	void signIn(mutex& usersMutex);
	void signOut(mutex& usersMutex);
	void connectedUsers(mutex& usersMutex);
	void printMenu();
	set<string> getUsers();

	set<string> _users;
};