#include "MessagesSender.h"



MessagerSender::MessagerSender()
{
	_users = {};
}

void MessagerSender::signIn(mutex& usersMutex)
{
	string name;
	set<string>::iterator it;
	cout << "Enter your name to log in: " << endl;
	cin >> name;

	

	if (_users.find(name) == _users.end())
	{
		cout << "Welcome " << name << endl;
		usersMutex.lock();
		_users.insert(name);
		usersMutex.unlock();
	}
	else
	{
		cout << "user already logged in" << endl;
	}
}



void MessagerSender::signOut(mutex& usersMutex)
{
	string name;
	set<string>::iterator it;
	cout << "Enter your name to log out: " << endl;
	cin >> name;



	if (_users.find(name) != _users.end())//if user exists
	{
		cout << "Logging out " << name << endl;
		usersMutex.lock();
		_users.erase(name);
		usersMutex.unlock();
	}
	else
	{
		cout << "user isn't logged in" << endl;
	}
}

void MessagerSender::connectedUsers(mutex& usersMutex)
{
	set<string>::iterator it;
	usersMutex.lock();
	for (it = _users.begin(); it != _users.end(); ++it)
	{
		cout << (*it) << "  ";
	}
	cout << endl;
	usersMutex.unlock();
}

void MessagerSender::printMenu()
{
	cout << "1. Sign in" << endl;
	cout << "2. Sign out" << endl;
	cout << "3. Connected users" << endl;
	cout << "4. Exit" << endl << endl;
}

set<string> MessagerSender::getUsers()
{
	return _users;
}