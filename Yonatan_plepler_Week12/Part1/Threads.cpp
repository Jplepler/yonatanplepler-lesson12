#include "threads.h"

mutex writeToFileMutex;


/*
This function prins the vector
In: vector of prime numbers
*/
void printVector(vector<int> primes)
{
	unsigned int i = 0;
	for (i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}


/*
This function writes all the primes from begin to end into a file
In: range of numbers, refrence to a file
*/
void writePrimesToFile(int begin, int end, ofstream& file)
{
	bool flag = true;
	int i = 0, k = 0;
	for (i = (begin > 2 ? begin : 2); i <= end; i++)//start form begining or 2 if begin is lower
	{
		flag = true;//Reset flag for next number check
		for (k = 2; k < sqrt(i) && flag; k++)
		{
			if (i % k == 0)
			{
				flag = false;//Not prime
			}
		}
		if (flag)
		{
			writeToFileMutex.lock();
			file << i << endl;
			writeToFileMutex.unlock();

		}
	}
}



/*
This function calls mutiple threads to write all prime number in a certain range to  a file
In: range, number of threads, relative path to txt file
*/
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	//clean file
	ofstream cleanfile(filePath);
	cleanfile.close();

	//
	ofstream file(filePath, std::fstream::in | std::fstream::out | std::fstream::app);
	vector<thread> threads;
	int segment = 0;
	int i = 0;
	unsigned int k = 0;

	//Divide into sections and put into vector
	for (i = 0; i < N; i++)
	{
		segment = begin + ((end) / N) * i;
		threads.push_back(thread(writePrimesToFile, segment, segment + (end - begin) / N, ref(file)));
	}

	for (k = 0; k < threads.size(); k++)
	{
		threads[k].join();
	}

	file.close();
}