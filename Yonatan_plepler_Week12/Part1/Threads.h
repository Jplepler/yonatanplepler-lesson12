#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <cmath>
#include <mutex>


using namespace std;



void printVector(vector<int> primes);
void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);
