#include "threads.h"



int main()
{
	//To files with threads
	cout << "Wrting to files functions with threads" << endl;
	auto start = chrono::high_resolution_clock::now();
	callWritePrimesMultipleThreads(0, 1000, "ThreadsAreWelcomeToWriteHere.txt", 10);
	auto stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;

	start = chrono::high_resolution_clock::now();
	callWritePrimesMultipleThreads(0, 100000, "ThreadsAreWelcomeToWriteHere.txt", 10);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;

	start = chrono::high_resolution_clock::now();
	callWritePrimesMultipleThreads(0, 1000000, "ThreadsAreWelcomeToWriteHere.txt", 10);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;

	

	system("pause");
	return 0;
}